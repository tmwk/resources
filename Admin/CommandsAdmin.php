<?php

namespace TMWK\ResourcesBundle\Admin;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Runroom\SortableBehaviorBundle\Admin\SortableAdminTrait;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CommandsAdmin extends AbstractAdmin
{
    use SortableAdminTrait;

    protected $datagridValues = array(
        '_page'       => 1,
        '_sort_order' => 'DESC',
        '_sort_by'    => 'id',
    );

    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('name', TextType::class)
            ->add('description', CKEditorType::class)
            ->add('rut', null)
            ->add('photo', ModelListType::class, ['label' => 'Imagen'], ['link_parameters' => ['context' => 'default', 'provider' => 'sonata.media.provider.image']])
            ->add('video', ModelListType::class, ['label' => 'Video'], ['link_parameters' => ['context' => 'default']]);
    }

    /*protected function configurePersistentParameters(): array
    {
        if (!$this->getRequest()) {
            return [];
        }

        return [
            'provider' => $this->getRequest()->get('provider'),
            'context'  => $this->getRequest()->get('context'),
        ];
    }*/

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('name');
    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('name')
            ->add(ListMapper::NAME_ACTIONS, ListMapper::TYPE_ACTIONS, [
                'label'              => 'Acciones',
                'translation_domain' => 'SonataAdminBundle',
                'actions'            => [
                    'show' => [],
                    'edit' => [],
                    'move' => [
                        'template'                  => '@RunroomSortableBehavior/sort.html.twig',
                        'enable_top_bottom_buttons' => false,
                    ],
                ],
            ]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('name');
    }

    /*protected function configureExportFields(): array
    {
        // Avoid sensitive properties to be exported.
        return array_filter(
            parent::configureExportFields(),
            static fn (string $v): bool => !\in_array($v, ['recaptcha', 'update_at', 'create_at'], true)
        );
    }*/


    protected function configureExportFields(): array
    {
        return array(
            'Nombre'      => 'name',
            'description' => 'description',
            'rut'         => 'rut',
            'photo'       => 'photo',
            'video'       => 'video',
        );
    }
}