<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: EncodesExtension.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 7/8/23 18:36
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\ResourcesBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class EncodesExtension extends AbstractExtension
{
    /**
     * @return array An array
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('serialize', [$this, 'encodeSerialize']),
            new TwigFilter('deserialize', [$this, 'encodeDeserialize']),
            new TwigFilter('json_encode', [$this, 'encodeJsonEncode']),
            new TwigFilter('json_decode', [$this, 'encodeJsonDecode']),
            new TwigFilter('base64_encode', [$this, 'encodeBase64Encode']),
            new TwigFilter('base64_decode', [$this, 'encodeBase64Decode']),
        ];
    }

    public function encodeSerialize($data): string
    {
        return serialize($data);
    }

    public function encodeDeserialize($data)
    {
        return unserialize($data ?: '');
    }

    public function encodeJsonEncode($data): string
    {
        return json_encode($data);
    }

    public function encodeJsonDecode($data)
    {
        return json_decode($data ?: '');
    }

    public function encodeBase64Encode($data): string
    {
        return base64_encode($data);
    }

    public function encodeBase64Decode($data)
    {
        return base64_decode($data ?: '');
    }


}