<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: CommandsController.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 4/8/23 22:52
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\ResourcesBundle\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

class CommandsController extends AbstractController
{
    public function __construct(private readonly KernelInterface $kernel)
    {

    }

    /**
     * @Route("/admin/commands", name="admin_commands")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('@TMWKResources/sonata/commands.html.twig', [

        ]);
    }

    /**
     * @throws Exception
     */
    private function sendCommand(array $input): void
    {
        define('STDIN', fopen("php://stdin", "r"));
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput($input);

        $output = new BufferedOutput();
        $application->run($input, $output);
        $content = $output->fetch();

        $this->addFlash(
            'sonata_flash_success', nl2br($content)
        );
    }


    /**
     * @Route("/admin/commands/clear_cache/{type}", name="admin_command_clear_cache")
     * @throws Exception
     */
    public function clearCacheAction($type = 'prod'): RedirectResponse
    {
        $this->sendCommand([
                               'command' => 'cache:clear',
                               '--env'   => $type,
                           ]);

        return $this->redirectToRoute('admin_commands');
    }

    /**
     * @throws Exception
     * @Route("/admin/commands/schema_update", name="admin_command_schema_update")
     */
    public function schemaUpdateAction(): RedirectResponse
    {
        $this->sendCommand([
                               'command'    => 'doctrine:schema:update',
                               '--force'    => true,
                               '--complete' => true,
                           ]);

        return $this->redirectToRoute('admin_commands');
    }

    /**
     * @throws Exception
     * @Route("/admin/commands/assets_install", name="admin_command_assets_install")
     */
    public function assetsInstallAction(): RedirectResponse
    {
        $this->sendCommand([
                               'command' => 'assets:install',
                           ]);

        return $this->redirectToRoute('admin_commands');
    }

    /**
     * @throws Exception
     * @Route("/admin/commands/entity_regenerates", name="admin_command_entity_regenerates")
     */
    public function entityRegeneratesAction(): RedirectResponse
    {
        $this->sendCommand([
                               'command'      => 'make:entity',
                               '--regenerate' => true,
                           ]);

        return $this->redirectToRoute('admin_commands');

    }

    /**
     * @throws Exception
     * @Route("/admin/commands/run_messenger", name="run_messenger")
     */
    public function runMessengerAction(): RedirectResponse
    {
        $command = 'symfony run -d --watch=config,src,templates,vendor symfony console messenger:consume async';
        $arguments = [];
        $logName = 'commands';
        $root_dir = '';
        $cmd = sprintf('%s/console %s', $root_dir, $command);
        if (count($arguments) > 0) {
            $cmd = sprintf($cmd . ' %s', implode(' ', $arguments));
        }
        $cmd = sprintf("%s --env=%s >> %s 2>&1 & echo $!", $cmd, 'dev', sprintf('%s/logs/%s.log', $root_dir, $logName));
        $process = new Process(['symfony run', ' -d --watch=config,src,templates,vendor symfony console messenger:consume async']);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new \RuntimeException($process->getErrorOutput());
        }

        $pid = $process->getOutput();
        
        dump($pid);
        exit;

        return $this->redirectToRoute('admin_commands');

    }
}
//symfony run -d --watch=config,src,templates,vendor symfony console messenger:consume async