<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: TMWKRedirectExtension.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 14:52
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\ResourcesBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class TMWKResourcesExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
