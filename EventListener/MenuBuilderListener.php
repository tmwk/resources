<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: MenuBuilderListener.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 5/8/23 0:22
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\ResourcesBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;
use Symfony\Component\HttpFoundation\RequestStack;

final class MenuBuilderListener
{

    public function __construct(private readonly RequestStack $request)
    {
    }

    public function addMenuItems(ConfigureMenuEvent $event): void
    {
        $menu = $event->getMenu();

        $routeName = $this->request->getCurrentRequest()->get('_route');

        $child = $menu->addChild('comandos', ['label' => 'Comandos', 'route' => 'admin_commands'])
            ->setExtras(['icon' => 'fas fa-key'])
            ->setCurrent($routeName == 'homepage2');
    }
}